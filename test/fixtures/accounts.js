[
  {
    "accountRefId" : "1111111",
    "lastFour" : "1234",
    "nickname" : "Primary Checking Account",
    "balance" : 125,
    "type" : "360 Checking"
  },
  {
    "accountRefId" : "2222222",
    "lastFour" : "5656",
    "nickname" : "Secondary Checking Account",
    "balance" : 720,
    "type" : "360 Checking"
  },
  {
    "accountRefId" : "444444444",
    "lastFour" : "8355",
    "nickname" : "Savings Account",
    "balance" : 9998,
    "type" : "360 Savings"
  }
]
