"use strict";

let fs = require("fs");
let expect = require("chai").expect;

let dbUrl = 'mongodb://localhost/alexa_payments';
let mongoose = require('mongoose');
let clearDB = require('mocha-mongoose')(dbUrl); //, {noClear: true});

describe("Contacts", function() {
  let contactService = require(__dirname + "/../app/features/contact/services/contact.service.js");

  beforeEach(function(done) {
    if(mongoose.connection.db) {
      return done();
    }

    mongoose.connect(dbUrl);

    fs.readFile(__dirname + "/fixtures/contacts.js", "utf8", function(err, contacts) {
      if(err) {
        console.error("Failed to load contacts fixture: ", err);
      } else {
        JSON.parse(contacts).forEach(function(contact) {
          contactService.create(contact);
        });
      }
      done();
    });
  });

  describe("findWhere", function() {
    it("firstname is fred", function(done) {
      return contactService.findByFirstName("fred").then(function(contacts) {
        expect(contacts).to.have.length(1);
        done();
      }, function(err) {
        done(err);
      });
    });
    it("firstname is empty", function(done) {
      return contactService.findByFirstName("").then(function(contacts) {
        done(new Error("should have returned an error"));
      }, function(err) {
        expect(err).to.be.an('error');
        done();
      });
    });
    it("firstname is not found", function(done) {
      return contactService.findByFirstName("bob").then(function(contacts) {
        expect(contacts).to.have.length(0);
        done();
      }, function(err) {
        done(err);
      });
    });
  });
});
