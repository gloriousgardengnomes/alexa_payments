'use strict';
let server = require('../app/server.js');

server.app.listen(server.port);

console.log('Server started on:', server.port);
