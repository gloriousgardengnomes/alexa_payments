'use strict';
let alexa = require('alexa-app'),
  alexaApp = new alexa.app('alexa');

alexaApp.launch((req, res) => {
  req.say('Welcome! Hello World.');
  // res.card('Hello World', 'This is an example card');
  // res.reprompt('What was that?');
});

alexaApp.sessionEnded((req, res) => {
  console.log('SESSION ENDED', req);
});


module.exports = alexaApp;
