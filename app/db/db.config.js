'use strict';

const fs = require('fs');

let mongoose = require('mongoose');
let mongoUrl = process.env.MONGO_URI || 'mongodb://localhost/alexa_payments';

mongoose.Promise = global.Promise;
mongoose.connect(mongoUrl);
let db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
  console.log('Connection to ' + mongoUrl + ' successfull!');
  setUpDefaults();
});

function setUpDefaults() {
  setUpContacts();
  setUpAccounts();

}

function setUpContacts() {
  let contactService = require('../features/contact/services/contact.service.js');
  contactService.removeContacts()
    .then(contacts => {
      fs.readFile(__dirname + '/../../test/fixtures/contacts.js', 'utf8', (err, contacts) => {
        if(err) {
          console.error('Failed to load contacts fixture: ', err);
        } else {
          JSON.parse(contacts).forEach(contact => {
            contactService.create(contact);
          });
        }
      });

    });
}

function setUpAccounts() {
  let accountService = require('../features/account/services/account.service.js');
  accountService.removeAccounts()
    .then(accounts => {
      fs.readFile(__dirname + '/../../test/fixtures/accounts.js', 'utf8', (err, accounts) => {
        if(err) {
          console.error('Failed to load accounts fixture: ', err);
        } else {
          JSON.parse(accounts).forEach(account => {
            accountService.createAccount(account);
          });
        }
      });

    });
}
