'use strict';
const fs = require('fs');

let express = require('express'),
  app = express(),
  bodyParser = require('body-parser'),
  alexaApp = require('./config/alexa/alexa.config.js'),
  verifier = require('alexa-verifier'),
  port = process.env.PORT || 8077,
  db = require('./db/db.config.js');


app.use((req, res, next) => {
  if(!req.headers.signaturecertchainurl) {
    return next();
  }
  // mark the request body as already having been parsed so it's ignored by
  // other body parser middlewares
  req._body = true;

  req.rawBody = '';
  req.on('data', (data) => {
    req.rawBody += data;
  });
  req.on('end', () => {
    try {
      req.body = JSON.parse(req.rawBody);
    } catch(err) {
      req.body = {};
    }

    let cert_url = req.headers.signaturecertchainurl,
      signature = req.headers.signature,
      requestBody = req.rawBody;

    verifier(cert_url, signature, requestBody, (err) => {
      if(err) {
        console.error('error validating the alexa cert:', err);
        res.status(401).json({
          status: 'failure',
          reason: err
        });
      } else {
        next();
      }
    });
  });
});

app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
app.use(bodyParser.json());
app.use(require('express-session')({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true
}));

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.post('/', (req, res) => {
  res.send({
    message: 'THIS IS AWESOMESAUCTASTIC!!!'
  });
});

fs.readdirSync(__dirname + '/features').forEach((feature) => {
  var intentsPath = __dirname + '/features/' + feature + '/intents/';

  if(fs.existsSync(intentsPath)) {
    fs.readdirSync(intentsPath).forEach((intent) => {
      require(intentsPath + intent)(alexaApp, app);
    });
  }
});

alexaApp.express(app, '/', true);
alexaApp.error = function(exception, req, res) {
  console.log('----------------------------\n',
    'Sorry, something bad happened\n', exception,
    '\n----------------------------');
};

module.exports = {
  app,
  port
};
