'use strict';

let paymentService = require('../../payment/services/payment.service');

module.exports = (alexaApp) => {

  alexaApp.intent('AMAZON.YesIntent', {},
    (req, res) => {

      switch(req.session('fromState')) {
        case 'forecast_bad':
          handleTransferLaunch(req, res);
          break;
        case 'TransferConfirm':
          handleTransferConfirm(req, res);
          break;
        case 'payments':
          handlePaymentConfirm(req, res);
          break;
        case 'WishList':
          handleWishListConfirm(req, res);
          break;
        default:
          let response = 'Sorry. For some reason I couldn\'t perform what you asked.';
          res.say(response).shouldEndSession(true).send();
      }

      return false;

    });

  alexaApp.intent('AMAZON.NoIntent', {},
    (req, res) => {
      res.say('Ok. Hope I can help you later.').shouldEndSession(true).send();
    });

  alexaApp.intent('Thanks', {},
    (req, res) => {
      res.say('No Prob, Bob.').shouldEndSession(true).send();
    });

};

function handlePaymentConfirm(req, res) {
  let phrase = 'Aye Aye Captain. ';
  let amount = req.session('amount');
  let account = req.session('account');
  let contact = req.session('contact');

  if(account && amount && contact) {

    let payment = {
      amount,
      from: 'Me',
      to: contact._id
    };

    paymentService.savePayment(account, amount, payment)
      .then(payment => {
        console.log(payment);
        let response = phrase + 'I just sent a payment of ' + amount + ' dollars to ' + contact.firstname + ' from ' +
            account.nickname + ' ending in <say-as interpret-as="characters">' + account.lastFour + '</say-as>';
        res.say(response).shouldEndSession(true).send();
      }, err => {
        console.error(err);
        let response = 'Sorry, something went wrong sending that payment. Would you like to try sending again?';
        res.say(response).shouldEndSession(false).send();
      });
  } else {
    let reprompt = 'Sorry, something went wrong sending that payment.';
    res.reprompt(reprompt).shouldEndSession(true).send();
  }
}

function handleTransferLaunch(req, res) {
  res.session('fromState', 'TransferAccount');
  let response = 'Which account would you like to transfer from?';
  res.say(response).shouldEndSession(false).send();

}

function handleTransferConfirm(req, res) {
  let transferAmount = req.session('transferAmount');
  let transferFromAccount = req.session('transferFromAccount');
  let response = 'OK. I just transfered ' + transferAmount + ' dollars from ' + transferFromAccount +
    ' to Primary Checking.';
  res.say(response).shouldEndSession(true).send();
}

function handleWishListConfirm(req, res) {
  let response = 'Alright, your order has been placed. Hope you enjoy your surprise!';
  res.say(response).shouldEndSession(false).send();
}
