'use strict';

let accountService = require('../../account/services/account.service');

module.exports = (alexaApp) => {

  alexaApp.intent('PayWith', {
    'slots': {
      lastFour: 'FOUR_DIGIT_NUMBER'
    },
    'utterances': [
      'pay with {lastFour}'
    ]
  }, (req, res) => {
    handlePayWithAccountsResponse(req, res);
    return false;
  });

};

function handlePayWithAccountsResponse(req, res) {
  let lastFour = req.slot('lastFour');
  accountService.findAccountByLastFour(lastFour)
    .then((account) => {
      res.session('account', account);
      res.say('Paying with account ending in <say-as interpret-as="characters">' + account.lastFour + '</say-as>, is that ok?')
        .shouldEndSession(false).send();
    });
}
