'use strict';

let accountService = require('../../account/services/account.service');
let contactService = require('../../contact/services/contact.service');

module.exports = (alexaApp) => {

  alexaApp.intent('Payments', {
    'slots': [{
      firstName: 'US_FIRST_NAME'
    }, {
      amount: 'NUMBER'
    }, {
      lastFour: 'FOUR_DIGIT_NUMBER'
    }],
    'utterances': [
      'to pay {firstName} {amount} {bucks|dollars|buckaroos}',
      'to pay {firstName} {amount} dollars from {lastFour}'
    ]
  }, (req, res) => {
    let firstName = req.slot('firstName');
    let amount = req.slot('amount');

    if(firstName && amount) {
      res.session('firstName', firstName);
      res.session('amount', amount);

      handleLaunchPayment(req, res);
    } else {
      handleLaunchPaymentFailure(req, res);
    }

    return false;
  });

};

function handleLaunchPaymentFailure(req, res) {
  let reprompt = '';
  if(!req.session('firstName')) {
    reprompt = 'Who would you like to send money to?  You can say something like, pay Bob ten bucks';
    res.reprompt(reprompt).shouldEndSession(false).send();
  } else if(!req.session('amount')) {
    reprompt = 'How much would you like pay?  You can say something like, pay Bob ten bucks.';
    res.reprompt(reprompt).shouldEndSession(false).send();
  }
}

function handleLaunchPayment(req, res) {
  let firstName = req.slot('firstName');

  contactService.findByFirstName(firstName)
    .then(contacts => {

      if(contacts.length >= 1) {
        handlePayUsingFirstName(req, res, contacts);
      } else {
        handlePayUsingAlias(req, res);
      }

    });

}

function handlePayUsingFirstName(req, res, contacts) {
  let phrase = '';
  let firstName = req.slot('firstName');

  if(contacts.length > 1) {
    phrase = 'Which ' + firstName + ' would you like to send to? ';

    for(let i = 0; i < contacts.length; i++) {
      if(i === 1) {
        //TODO: make a full name method/contstructor in contact
        phrase += contacts[i].firstname + ' ' + contacts[i].lastname;
      } else {
        phrase += 'or ' + contacts[i].firstname + ' ' + contacts[i].lastname;
      }
    }

  } else {
    res.session('contact', contacts[0]);
    res.session('fromState', 'payments');

    return handlePaymentCompletion(req, res, firstName);
  }

  res.say(phrase).shouldEndSession(false).send();
}

function handlePayUsingAlias(req, res) {
  let alias = req.slot('firstName');
  contactService.findByAlias(alias)
    .then(contact => {
      //contact === null if not found
      if(contact) {
        res.session('contact', contact);
        handlePaymentCompletion(req, res, alias);
      } else {
        let phrase = 'Sorry, but none of your peeps were found with name ' + alias + '. Please try again.';
        res.say(phrase).shouldEndSession(false).send();
      }
    });
}

function handlePaymentCompletion(req, res, name) {
  let phrase = '';
  let amount = req.slot('amount');
  let lastFour = req.slot('lastFour');

  accountService.findAccounts()
    .then(accounts => {
      if(accounts.length === 1 && !lastFour) {
        res.session('account', accounts[0]);
        phrase = 'Paying ' + name + ' ' + amount +
          ' dollars with account ending in <say-as interpret-as="characters">' + accounts[0].lastFour + '</say-as>. Proceed?';
      } else if(lastFour) {
        let account;
        accounts.forEach(acct => {
          if(acct.lastFour == lastFour) {
            account = acct;
          }
        });
        if(account) {
          res.session('account', account);
          phrase = 'Paying ' + name + ' ' + amount +
            ' dollars with account ending in <say-as interpret-as="characters">' + account.lastFour + '</say-as>. Proceed?';
        } else {
          phrase = 'Sorry, but I couldn\'t find any account ending in ' + lastFour + '. Please try again.';
        }
      } else {
        phrase = 'Which account would  you like to pay ' + name +
          ' with? Just say the last four digits of the account.';
      }

      res.say(phrase).shouldEndSession(false).send();
    });
}
