'use strict';
let accountService = require('../../account/services/account.service');
let Payment = require('../models/payment.model');

let PaymentsService = {

  savePayment(account, amount, payment) {
    return accountService.updateAccountBalanceByLastFour(account.lastFour, amount)
      .then(() => {
        return new Payment(payment).save();
      }, (err) => {
        console.log(err);
        return new Promise((resolve, reject) => {
          reject(new Error('Could not complete payment.'));
        });
      });

  }
};

module.exports = PaymentsService;
