'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var PaymentSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  amount: {
    type: Number,
    required: 'Amount cannot be blank'
  },
  from: {
    type: String,
    trim: true
  },
  to: {
    type: Schema.Types.ObjectId,
    ref: 'Contact'
  }
});

module.exports = mongoose.model('Payment', PaymentSchema);
