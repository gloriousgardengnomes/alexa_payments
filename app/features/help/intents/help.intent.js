'use strict';

module.exports = (alexaApp) => {

  let cardObj = {
    type: 'Standard',
    title: 'Mr. Jeeves',
    text: 'Jeeves: A personal bank butler for all your payment needs',
    image: {
      largeImageUrl: 'https://s3.amazonaws.com/hackathon-alexa/jeeves1.jpg',
      smallImageUrl: 'https://s3.amazonaws.com/hackathon-alexa/jeeves1.jpg'
    }
  };

  alexaApp.launch(function(req, res) {
    res.say('Welcome to Jeeves. You can say Help at anytime to list the skills Mr. Jeeves has available');
    res.card('Jeeves', cardObj);
  });

  alexaApp.intent('magic', function(request, response) {
    var number = request.slot('number');
    response.say('You asked for the number ' + number);
  });


  alexaApp.intent('Amazon.HelpIntent', (req, res) => {
    res.say('You can say things like.  Pay Fred ten bucks, list my accounts, what is my account balance, whats my' +
      ' funds forecast');
  });


};
