'use strict';
module.exports = alexaApp => {

  let pickArry = ['sunny', 'cloudy'];

  alexaApp.intent('FundsForecast', {
    'utterances': [
      'whats my funds forecast for the the end of this month',
      'how is the weather looking at the end of this month'
    ]
  }, (req, res) => {

    // let item = pickArry[Math.floor(Math.random() * pickArry.length)];
    let item = 'sunny';

    if(item === 'sunny') {
      res.session('fromState', 'forecast_good');
      res.say('Your funds forecast is looking sunny.  You should be up 100 dollars at the end of the month. ' +
          'Do you want to play shopping roulette?')
        .shouldEndSession(false).send();
    } else {
      res.session('fromState', 'forecast_bad');
      res.say('Your funds forecast appears to be cloudy.  You\'ll be minus 30 dollars at the end of the month. ' +
          'Would you like to move some money into your Primary Checking Account?')
        .shouldEndSession(false).send();
    }

    return false;
  });

  alexaApp.intent('Roulette', {
    'utterances': [
      '{yes up|up} to {rouletteAmount} {dollars|bucks}',
    ]
  }, (req, res) => {
    res.session('rouletteAmount', req.slot('rouletteAmount'));
    res.say('What wish list would you like to use?')
      .shouldEndSession(false).send();
  });


  alexaApp.intent('WishList', {
    'utterances': [
      'use {owner} wish list please',
      'use {owner} wish list'
    ]
  }, (req, res) => {
    let amount = req.session('rouletteAmount');
    let owner = req.slot('owner');
    res.session('fromState', 'WishList');
    res.say('To confirm, you want to use the wish list for ' + owner + ' for shopping roulette up to $' + amount + '?')
      .shouldEndSession(false).send();
  });

};
