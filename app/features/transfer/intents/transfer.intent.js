'use strict';

module.exports = alexaApp => {

  alexaApp.intent('TransferAccount', {
    'utterances': []
  }, (req, res) => {
    handleTransferAccountResponse(req, res);
    return false;
  });

  alexaApp.intent('TransferAmount', {
    'utterances': []
  }, (req, res) => {
    handleTransferAmountResponse(req, res);
    return false;
  });

  alexaApp.intent('TransferConfirm', {
    'utterances': []
  }, (req, res) => {
    handleTransferConfirmResponse(req, res);
    return false;
  });

  function handleTransferAccountResponse(req, res) {
    if(req.session('fromState') === 'forecast_bad') {
      res.session('fromState', 'TransferAccount');

      let response = 'Which account would you like to transfer from?';
      res.say(response).shouldEndSession(false).send();
    } else {
      let response = 'Sorry. That feature isn\'t available at this time.';
      res.say(response).shouldEndSession(false).send();
    }
  }

  function handleTransferAmountResponse(req, res) {
    if(req.session('fromState') === 'TransferAccount') {
      let account = 'primary savings';
      res.session('transferFromAccount', account);
      res.session('fromState', 'TransferAmount');

      let response = 'How much?';
      res.say(response).shouldEndSession(false).send();
    } else {
      let response = 'Sorry. That feature isn\'t available at this time.';
      res.say(response).shouldEndSession(false).send();
    }
  }

  function handleTransferConfirmResponse(req, res) {
    if(req.session('fromState') === 'TransferAmount') {
      res.session('transferAmount', req.slot('amt'));
      res.session('fromState', 'TransferConfirm');

      let response = 'Are you sure you want to transfer ' + req.slot('amt') +
        ' dollars to ' + req.session('transferFromAccount') + '?';
      res.say(response).shouldEndSession(false).send();
    } else {
      let response = 'Sorry. That feature isn\'t available at this time.';
      res.say(response).shouldEndSession(false).send();
    }
  }

};
