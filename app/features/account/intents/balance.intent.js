'use strict';

let accountService = require('../services/account.service');

module.exports = (alexaApp) => {

  alexaApp.intent('Balance', {
    'slots': [{
      lastFour: 'FOUR_DIGIT_NUMBER'
    }, {
      nickname: 'string'
    }],
    'utterances': [
      'what is my balance',
      'what my balance is for {lastFour}',
      'what is my balance for {lastFour}',
    ]
  }, (req, res) => {
    let lastFour = req.slot('lastFour');

    if(!lastFour) {
      accountService.findAccounts()
        .then(accounts => {
          if(accounts.length === 1) {
            handleBalanceResponse(req, res, accounts[0]);
          } else if(accounts.length > 1) {
            let repromptMsg = 'Since you have more than one account, please specify the account you would like to use. ' +
              'You can say something like, what is my balance for <say-as interpret-as="characters">1234</say-as>, ' +
              'using the last four digits of your account.';

            res.reprompt(repromptMsg).shouldEndSession(false).send();
          } else {
            let repromptMsg = 'Sorry, I didn\'t find any available accounts.';

            res.reprompt(repromptMsg).shouldEndSession(true).send();
          }
        })
        .catch(err => {
          console.error(err);
        });
    } else if(lastFour) {
      accountService.findAccountByLastFour(lastFour)
        .then(account => {
          handleBalanceResponse(req, res, account);
        })
        .catch(err => {
          handleNoAccountWithLastFourResponse(res, lastFour, err);
        });
    } else {
      let repromptMsg = 'Sorry, I didn\'t get that. You can say something like, ' +
        'what is my balance for <say-as interpret-as="characters">1234</say-as>, using the last four digits of your account.';
      res.reprompt(repromptMsg).shouldEndSession(false).send();
    }

    return false;
  });

};

function handleBalanceResponse(req, res, account) {
  let response = 'The balance for account ending in <say-as interpret-as="characters">' + account.lastFour +
    '</say-as> is, $' + account.balance;
  let sessionShouldEnd = req.session('fromState') === 'payments' ? false : true;

  res.say(response).shouldEndSession(sessionShouldEnd).send();
}

function handleNoAccountWithLastFourResponse(res, slot) {
  let response = 'Sorry, I couldn\'t find an account for account ending in ' + slot + '';

  res.say(response).shouldEndSession(false).send();
}
