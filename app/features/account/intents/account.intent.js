'use strict';

let accountService = require('../services/account.service');

module.exports = (alexaApp) => {

  alexaApp.intent('Accounts', {
    'slots': [],
    'utterances': [
      'what are my accounts',
      'what my accounts are',
      'to list my accounts',
      'for a list of my accounts',
      'for my accounts',
      'accounts'
    ]
  }, (req, res) => {
    handleListAccountsResponse(req, res);
    return false;
  });

};

function handleListAccountsResponse(req, res) {
  accountService.findAccounts()
    .then(accounts => {
      let response = '';
      let sessionShouldEnd = req.session('fromState') === 'payments' ? false : true;

      accounts.forEach(account => {
        response += account.nickname + ', ending in <say-as interpret-as="characters">' + account.lastFour + '</say-as>';
      });

      res.say(response).shouldEndSession(sessionShouldEnd).send();
    })
    .catch(err => {
      console.error(err);
      res.reprompt('Sorry, I couldnt find any accounts for you.').shouldEndSession(true).send();
    });
}
