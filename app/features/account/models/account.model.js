'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var AccountSchema = new Schema({
  type: {
    type: String,
    default: '',
    trim: true
  },
  balance: {
    type: Number,
    default: 0
  },
  nickname: {
    type: String,
    default: '',
    trim: true,
  },
  lastFour: {
    type: String,
    default: '',
    trim: true
  },
  accountRefId: {
    type: String,
    default: '',
  },
});

module.exports = mongoose.model('Account', AccountSchema);
