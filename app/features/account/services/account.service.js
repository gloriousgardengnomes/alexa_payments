'use strict';

let Account = require('../models/account.model.js');

let AccountService = {

  findAccounts() {
    //Get all accounts
    return Account.find({});
  },

  removeAccounts() {
    //Delete all accounts
    return Account.remove({});
  },

  //Individual Account queries
  createAccount(account) {
    new Account(account).save((err) => {
      if(err) {
        console.error('Failed to save account', account.lastFour);
      }
    });
  },

  findAccountByLastFour(lastFour) {
    return Account.findOne({
      lastFour
    });
  },

  findAccountByNickname(nickname) {
    return Account.findOne({
      nickname
    });
  },

  //Balance queries
  updateAccountBalanceByLastFour(lastFour, amount) {
    let balanceUpdatePromise = new Promise((resolve, reject) => {
      Account.findOne({
        lastFour: lastFour
      }, (err, account) => {
        if(err) {
          reject(err);
        } else {
          let newBalance = account.balance - amount;
          account.set('balance', newBalance);
          account.save((err, account) => {
            if(err) {
              console.error(err);
              reject(err);
            } else {
              console.log('New balance for account ending in ' + account.lastFour + ': ' + newBalance);
              resolve(account);
            }
          });
        }
      });
    });

    return balanceUpdatePromise;
  },

};

module.exports = AccountService;
