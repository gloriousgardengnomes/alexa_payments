'use strict';

let Contact = require('../models/contact.model');

var contactService = {

  /**
   * Persist a new contact instance
   */
  create(contact) {
    return new Contact(contact).save(contact);
  },

  removeContacts() {
    return Contact.remove({});
  },

  /**
   * Retrieve a list of possible contacts by first name (case insensitive).
   *
   * @param fname
   *        the first name of the contact to lookup
   * @returns {Promise}
   */
  findByFirstName(fname) {
    if(fname) {
      return Contact.find({ firstname: new RegExp(fname, 'i') });
    }
    return new Promise(function(resolve, reject) {
      reject(new Error('First name is required'));
    });
  },

  findByAlias(alias) {  
    //Aliases must be unique, so we can use findOne here
    return Contact.findOne({ 'alias.name': new RegExp(alias, 'i') });
  }
};

module.exports = contactService;
