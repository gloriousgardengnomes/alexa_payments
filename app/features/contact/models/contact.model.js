'use strict';

var mongoose = require('mongoose'),
  AliasSchema = require('./alias.model.js'),
  Schema = mongoose.Schema;

var ContactSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  firstname: {
    type: String,
    default: '',
    trim: true,
    required: 'First name cannot be blank'
  },
  lastname: {
    type: String,
    default: '',
    trim: true
  },
  phone: {
    type: String,
    default: '',
    trim: true
  },
  email: {
    type: String,
    default: '',
    trim: true
  },
  alias: [AliasSchema]
});

module.exports = mongoose.model('Contact', ContactSchema);