'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var AliasSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  name: {
    type: String,
    default: '',
    trim: true
  }
});

mongoose.model('Alias', AliasSchema);